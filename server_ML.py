import flask
import pandas as pd
from flask import request, jsonify
from model_1 import Model1
from model_2 import Model2

model_list={"Model1":Model1,"Model2":Model2}

app = flask.Flask(__name__)
app.config["DEBUG"] = True

for model_id, _ in model_list.items():
    @app.route("/api/v1/{}/train".format(model_id), endpoint='func_fit_{}'.format(model_id), methods=["GET"])
    def api_fit_model():
        if 'db_id' in request.args:
            db_id = str(request.args['db_id'])
        else:
            return "Error: db_id not provided"
        model = model_list[model_id]()
        requirements = model.requirements
        #todo: get data from DB
        db_data = None
        train_data = pd.DataFrame(db_data)
        model.fit(train_data)
        return "model trained"

    @app.route("/api/v1/{}/test".format(model_id), endpoint='func_predict_{}'.format(model_id), methods=["GET"])
    def api_predict_point():
        test_vector = {}
        for feat in request.args:
            test_vector[feat] = float(request.args[feat])
        model = model_list[model_id]()
        if test_vector:
            results = model.predict([test_vector])
        else:
            results = "No test vector provided"
        return results

app.run()
