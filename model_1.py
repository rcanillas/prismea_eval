from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from kafka import KafkaProducer
from json import dumps


class Model1:
    """
    todo:finish the docstring
    """
    def __init__(self):
        self.requirements = ["date"]
        self.default_parameters = {}
        self.is_trained = False
        self.model = RandomForestClassifier(random_state=0)

    def fit(self, train_data):
        feats = train_data[[c_name for c_name in train_data.columns if c_name != "label"]]
        labels = train_data["label"]
        self.model.fit(feats, labels)
        self.is_trained = True
        return self

    def predict(self, test_data):
        if not self.is_trained:
            return False
        output = self.model.predict(test_data)
        return output


if __name__ == '__main__':
    import pandas as pd
    test_model = Model1()
    X, y = make_classification(n_samples=1000, n_features=4,
                               n_informative=2, n_redundant=0,
                               random_state=0, shuffle=False)
    train_data = pd.DataFrame(X)
    train_data["label"] = y
    test_model.fit(train_data)
    result = test_model.predict([[0, 0, 0, 0]])
    print(result)